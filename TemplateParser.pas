unit TemplateParser;

interface
uses RegExpUtil, SysUtils;

type
  TStringFunction = function(value: string): string of object;
  ETemplateParser = class(Exception);

  (***************************************************************************
    ����� ������������ ��� ������� ��������, ��������� �� ������ � �����������
    ��������, ��������� ������������� ��������������. ��� ������ ���������
    ���������� ������� OnDirective, ��������� ������������� ������ ���������
    (������������, ������� ��, ���� ���������)

    ������� (���������� ������� �� ��������, ����������� ����):

    t := TOeTemplateParser.Create('<\?\s*', '\s*\?>', self.ToUpperCase);
    ��������� ������ t.Parse('������<?   ������   ?>������'): ������������������

    t := TOeTemplateParser.Create('\{\{', '\}\}', self.ReverceString);
    ��������� ������ t.Parse('12345{{  12345 }}12345'): 12345 54321  12345
    ***************************************************************************)
  TTemplateParser = class
  private
    fDirectiveBeginDelimiter, fDirectiveEndDelimiter: TRegExp;
    fOnDirective: TStringFunction;
    FIncludeDelimiters: boolean;
  protected
  public
    Constructor Create(OpenDirectiveRegexp: string;
      CloseDirectiveRegexp: string; OnDirective: TStringFunction);
    destructor Destroy; override;
    property IncludeDelimiters: boolean read FIncludeDelimiters write FIncludeDelimiters;
    function Parse(Source: string): string;
  end;

implementation
uses
  Classes, RegExpr;

{ TOeTemplateParser }

constructor TTemplateParser.Create(OpenDirectiveRegexp,
  CloseDirectiveRegexp: string; OnDirective: TStringFunction);
begin
  fDirectiveBeginDelimiter := TRegExp.Create(OpenDirectiveRegexp);
  fDirectiveEndDelimiter := TRegExp.Create(CloseDirectiveRegexp);
  fOnDirective := OnDirective;
end;

destructor TTemplateParser.Destroy;
begin
  fDirectiveEndDelimiter.Free;
  fDirectiveBeginDelimiter.Free;
  inherited;
end;

function TTemplateParser.Parse(Source: string): string;
var
  sb: TStringStream;
  PlainText, Directive, DirectiveResult: string;
  CurPos: integer;
begin
  sb := TStringStream.Create('');
  try
    CurPos := 1;
    fDirectiveBeginDelimiter.InputString := Source;
    fDirectiveEndDelimiter.InputString := Source;
    while true do
    begin
      if fDirectiveBeginDelimiter.ExecPos(CurPos) then
      begin
        PlainText := copy(Source, CurPos, fDirectiveBeginDelimiter.MatchPos[0] - CurPos);
        CurPos := fDirectiveBeginDelimiter.MatchPos[0]
            + fDirectiveBeginDelimiter.MatchLen[0];
        Sb.WriteString(PlainText);
        if IncludeDelimiters then
          sb.WriteString(fDirectiveBeginDelimiter.Match[0]);
      end
      else begin
        PlainText := copy(Source, CurPos, MaxInt);
        Sb.WriteString(PlainText);
        break;
      end;
      if fDirectiveEndDelimiter.ExecPos(CurPos) then
      begin
        Directive := copy(Source, CurPos, fDirectiveEndDelimiter.MatchPos[0] - CurPos);
        CurPos := fDirectiveEndDelimiter.MatchPos[0]
            + fDirectiveEndDelimiter.MatchLen[0];
        DirectiveResult := fOnDirective(Directive);
        Sb.WriteString(DirectiveResult);
        if IncludeDelimiters then
          sb.WriteString(fDirectiveEndDelimiter.Match[0]);
      end
      else begin
        Directive := copy(Source, CurPos, MaxInt);
        raise ETemplateParser.CreateFmt('%s: ���������� ��������� �%s�',
            [ClassName, Directive])
      end;
    end;
    result := sb.DataString;
  finally
    sb.Free;
  end;
end;

end.

program replaceBetween;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  TemplateParser in 'TemplateParser.pas',
  RegexpUtil in 'RegexpUtil.pas',
  Classes;

type
  TBetweenParser = class(TTemplateParser)
  private
    fReplacer: string;
    function Between(piece: string): string;
  public
    Constructor Create(OpenDirectiveRegexp: string;
      CloseDirectiveRegexp: string);
    function Parse(Source, ReplaceTo: string): string;
  end;

{ TBetweenParser }

function TBetweenParser.Between(piece: string): string;
begin
  result := fReplacer;
end;

constructor TBetweenParser.Create(OpenDirectiveRegexp,
  CloseDirectiveRegexp: string);
begin
  inherited Create(OpenDirectiveRegexp, CloseDirectiveRegexp, Between);
end;

var
  Src, Ins: string;
  parser: TBetweenParser;

function TBetweenParser.Parse(Source, ReplaceTo: string): string;
begin
  fReplacer := ReplaceTo;
  result := TTemplateParser(self).Parse(Source);
end;

{ local functions }

function readFromFile(fileName: string): string;
var
  fs: TFileStream;
  ss: TStringStream;
begin
  ss := TStringStream.Create('');
  try
    fs := TFileStream.Create(fileName, fmOpenRead + fmShareDenyNone);
    try
      ss.CopyFrom(fs, fs.Size);
      result := ss.DataString;
    finally
      fs.Free;
    end;
  finally
    ss.Free;
  end;
end;

begin
  if ParamCount < 4 then
  begin
    Writeln('Usage: between sourceFile FileForInsert startregexp endregexp');
    Writeln('Note: used TRegExpr for Delphi 7');
    Writeln('You can use #regexpr#modificators for more control');
    exit;
  end;

  src := readFromFile(ParamStr(1));
  ins := readFromFile(ParamStr(2));
  parser := TBetweenParser.Create(ParamStr(3), ParamStr(4));
  try
    parser.IncludeDelimiters := true;
    Write(parser.Parse(src, ins));
  finally
    parser.Free;
  end;

end.

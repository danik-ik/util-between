program between;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  TemplateParser in 'TemplateParser.pas',
  RegexpUtil in 'RegexpUtil.pas',
  Classes;

type
  TBetweenParser = class(TTemplateParser)
  private
    function Between(piece: string): string;
  public
    Constructor Create(OpenDirectiveRegexp: string;
      CloseDirectiveRegexp: string);
  end;

{ TBetweenParser }

function TBetweenParser.Between(piece: string): string;
begin
  Write(piece);
end;

var
  src: TStringStream;
  fs: TFileStream;
  parser: TBetweenParser;
constructor TBetweenParser.Create(OpenDirectiveRegexp,
  CloseDirectiveRegexp: string);
begin
  inherited Create(OpenDirectiveRegexp, CloseDirectiveRegexp, Between);
end;

begin
  if ParamCount < 3 then
  begin
    Writeln('Usage: between filename startregexp endregexp');
    Writeln('Note: used TRegExpr for Delphi 7');
    Writeln('You can use #regexpr#modificators for more control');
    exit;
  end;

  Src := TStringStream.Create('');
  try
    fs := TFileStream.Create(ParamStr(1), fmOpenRead + fmShareDenyNone);
    try
      src.CopyFrom(fs, fs.Size);
    finally
      fs.Free;
    end;
    parser := TBetweenParser.Create(ParamStr(2), ParamStr(3));
    try
      parser.Parse(src.DataString);
    finally
      parser.Free;
    end;
  finally
    Src.Free;
  end;
end.

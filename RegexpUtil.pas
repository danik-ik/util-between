unit RegexpUtil;

interface
uses
  RegExpr;

type
  ERegexpUtil = class(ERegExpr);

  TRegExp = class (TRegExpr)
  public
    constructor Create(FullExpression: string; QuotationMark: char = '#');
      overload;
      // - FullExpression = '#expression#modificators'
  end;

function StringMatchRegexp(InputString, Expression: String): boolean;
function GetRegexpMatch(InputString, Expression: String;
  matchIndex: integer = 0): string;

implementation

{ TRegExp }

constructor TRegExp.Create(FullExpression: string; QuotationMark: char = '#');
var
  i: integer;
begin
  inherited Create;
  if FullExpression = '' then exit;
  // Yes, I do it without regular expressions!!!
  if FullExpression[1] <> QuotationMark then // unquoted expression
    self.Expression := FullExpression
  else begin // Quoted expression
    i := length(FullExpression);
    while ((i > 1) and (FullExpression[i] <> QuotationMark)) do dec(i);
    if i = 1 then
      self.Expression := ''
    else
      self.Expression := Copy(FullExpression, 2, i-2);
    self.ModifierStr := Copy(FullExpression, i+1, length(FullExpression))
  end
end;

function StringMatchRegexp(InputString, Expression: String): boolean;
var Regexp: TRegExp;
begin
  Regexp := TRegExp.Create(Expression);
  try
    Result := Regexp.Exec(InputString);
  finally
    Regexp.Free;
  end;
end;

function GetRegexpMatch(InputString, Expression: String;
  matchIndex: integer = 0): string;
var Regexp: TRegExp;
begin
  Regexp := TRegExp.Create(Expression);
  try
    result := '';
    if Regexp.Exec(InputString) then
      result := regexp.Match[matchIndex];
  finally
    Regexp.Free;
  end;
end;

end.
